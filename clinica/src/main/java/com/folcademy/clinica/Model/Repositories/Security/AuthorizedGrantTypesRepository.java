package com.folcademy.clinica.Model.Repositories.Security;

import com.folcademy.clinica.Model.Entities.Security.AuthorizedGrantTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizedGrantTypesRepository extends JpaRepository<AuthorizedGrantTypes,Integer> {
}
