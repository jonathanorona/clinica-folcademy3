package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10)")
    public Integer idpaciente;
    @Column(name = "dni")
    public String dni;
    @Column(name = "Nombre")
    public String nombre;
    @Column(name = "Apellido")
    public String apellido;
    @Column(name = "Telefono")
    public String telefono;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Paciente paciente = (Paciente) o;
        return idpaciente != null && Objects.equals(idpaciente, paciente.idpaciente);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
