package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnosController {

    private final TurnoService turnoService;

    public TurnosController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    //Listar turnos
    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping(value = "")
    public ResponseEntity<List<Turno>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAllTurnos())
                ;
    }

    //create turno
    @PreAuthorize("hasAuthority('post_turno')")
    @PostMapping("")
    public ResponseEntity<?> crearTurno(@RequestBody TurnoDTO turnoDTO) {
        return turnoService.crearTurno(turnoDTO);
    }
}
