package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Model.Dtos.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TurnoService implements ITurnoService {

    private final TurnoRepository turnoRepository;

    public TurnoService(TurnoRepository turnoRepository) {
        this.turnoRepository = turnoRepository;
    }

    //listar Turnos
    @Override
    public List<Turno> findAllTurnos() {
        return (List<Turno>) turnoRepository.findAll();
    }

    //crear Turno
    public ResponseEntity<?> crearTurno(TurnoDTO turnoDTO){
        if (turnoDTO.getFecha() == null){
            throw new BadRequestException("La fecha es obligatoria");
        }

        Turno turnoEntity = new Turno();
        turnoEntity.setFecha(turnoDTO.getFecha());
        turnoEntity.setHora(turnoDTO.getHora());
        turnoEntity.setAtendido(turnoDTO.getAtendido());
        turnoEntity.setIdpaciente(turnoDTO.getIdpaciente());
        turnoEntity.setIdmedico(turnoDTO.getIdmedico());

        turnoRepository.save(turnoEntity);

        return new ResponseEntity<>(turnoEntity, HttpStatus.OK);
    }
}
